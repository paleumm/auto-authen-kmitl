import argparse
import getpass
import json
import os
import shutil
import signal
import time
import requests
import sys


username = ''
password = ''
time_repeat = 10  # seconds
max_login_attempt = 20

client_account = ''
client_ip = ''
client_token = ''
server_url = 'http://connect.kmitl.ac.th:8080'

agent = requests.session()


# handle Ctrl+C
def signal_handler(signal, frame):
    running = (client_token != '')
    if not running:
        print('\nGood bye!')
        sys.exit(0)
    print_format('Good bye!', end='\n')
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)


# determine terminal size
large_terminal = True
column, line = shutil.get_terminal_size()
if column < 108:
    large_terminal = False


# arguments parser
parser = argparse.ArgumentParser(
    description='Automatically authenticate into KMITL network system.')
parser.add_argument('-u', '--username', dest='username',
                    help='username without @kmitl.ac.th (usually student ID)')
parser.add_argument('-p', '--password', dest='password', help='password')
parser.add_argument('-i', '--interval', dest='interval', type=int,
                    help='heartbeat interval in second (default: {} seconds)'.format(time_repeat))
parser.add_argument('--max-login-attempt', dest='max_attempt', type=int,
                    help='maximum login attempt (default: {} times)'.format(max_login_attempt))
parser.add_argument('--config', dest='config', action='store_const',
                    const=True, default=False, help='create config file')

def print_format(*args, large_only=False, small_only=False, show_time=True, end='\n\n', **kwargs):
    if (large_only and not large_terminal) or (small_only and large_terminal):
        return

    if large_terminal:
        print('\t', end='')
    if show_time:
        print(time.asctime(time.localtime()), '[x]', end=' ')
    print(*args, **kwargs, end=end)


def print_error(*args, **kwargs):
    print_format(*args, **kwargs, end='\n')
    #sys.exit(1)


def init():
    logo = '''
        ****************************************************************************************************
        *  _   _      _                      _      _           _                     _                    *
        * | \ | |    | |                    | |    | |         | |                   | |                   *
        * |  \| | ___| |___      _____  _ __| | __ | |     __ _| |__   ___  _ __ __ _| |_ ___  _ __ _   _  *
        * | . ` |/ _ \ __\ \ /\ / / _ \| '__| |/ / | |    / _` | '_ \ / _ \| '__/ _` | __/ _ \| '__| | | | *
        * | |\  |  __/ |_ \ V  V / (_) | |  |   <  | |___| (_| | |_) | (_) | | | (_| | || (_) | |  | |_| | *
        * |_| \_|\___|\__| \_/\_/ \___/|_|  |_|\_\ |______\__,_|_.__/ \___/|_|  \__,_|\__\___/|_|   \__, | *
        *                                                                                            __/ | *
        *                                                                                           |___/  *
        *                                                                                                  *
        ****************************************************************************************************'''
    print_format(logo, large_only=True, show_time=False)
    print_format('\nNetwork Laboratory', small_only=True, show_time=False)


def checkConnection() -> (bool, bool):
    global server_url
    try:
        content = requests.get('http://detectportal.firefox.com/success.txt')
    except requests.exceptions.RequestException:
        return False, False
    if content.text == 'success\n':
        return True, True
    html = content.text
    start = html.find('URL=') + 4
    end = html.find('/portal')
    server_url = html[start:end]
    return True, False


def login():
    global client_account
    global client_ip
    global client_token

    try:
        url = server_url + '/PortalServer/Webauth/webAuthAction!login.action'
        content = agent.post(url,params={'userName': username, 'password': password})
    except requests.exceptions.RequestException:
        print_format('Connection lost...')
        time.sleep(1)
        return
    content_dict = json.loads(content.text)

    # check if request is successful
    if not content_dict['success']:
        print_error('Error! Something went wrong (maybe too many attempt?)...')

    token = content_dict['token']
    data = content_dict['data']

    # get value from response form
    # ip - token - account
    client_ip = data['ip']
    client_account = data['account']
    client_token = token[6:]

    if data['accessStatus'] != 200:
        print_error(
            'Error! Something went wrong (maybe wrong username and/or password?)...')


def heatbeat() -> (bool, bool):
    try:
        url = server_url + '/PortalServer/Webauth/webAuthAction!hearbeat.action'
        content = agent.post(url,params={'userName': client_account}, headers={'X-XSRF-TOKEN': client_token})
    except requests.exceptions.RequestException:
        print_format('Connection lost...')
        time.sleep(1)
        return False, False
    content_dict = json.loads(content.text)
    status = content_dict['data']
    if status == 'ONLINE':
        print_format('Heatbeat OK...')
        return True, True
    else:
        print_format('Heatbeat failed...')
        return True, False


def start():
    connection, internet = checkConnection()
    login()
    reset_timer = time.time()+(8*60*60)
    login_attempt = 0
    printed_logged_in = False

    while True:
        remain_to_reset = reset_timer-time.time()
        connection, internet = checkConnection()
        if remain_to_reset <= 480:
            return
        if connection and internet:
            # successful
            if not printed_logged_in:  # print only when log in successful
                print_format('Welcome {}!'.format(client_account), end='\n')
                print_format('Your IP:', client_ip, end='\n')
                print_format('Token:', client_token)
                print_format('Heatbeat every', time_repeat, 'seconds')
                print_format('Max login attempt:', max_login_attempt)
                printed_logged_in = True

            time.sleep(time_repeat)
            connection, heatdone = heatbeat()
            if not connection and not heatdone:
                return
            elif connection and not heatdone:
                login()
        elif connection and not internet:
            if login_attempt == max_login_attempt:
                print_error(
                    'Error! Please recheck your username and password...')
            login()
            login_attempt += 1
        else:
            print_format('Connection lost...')
            time.sleep(1)
            return


def create_config():
    input_username = input('Your username (mostly student ID): ')
    input_password = getpass.getpass('Your password: ')
    try:
        input_interval = int(input('Heartbeat interval ({} sec): '.format(time_repeat)))
    except ValueError:
        input_interval = time_repeat

    data = {}
    if input_username != '':
        data.update({'username': input_username})
    if input_password != '':
        data.update({'password': input_password})
    data.update({'interval': input_interval})

    with open('config.json', 'w') as config_file:
        json.dump(data, config_file, indent=4)


if __name__ == '__main__':
    # get arguments
    args = parser.parse_args()

    # check for creating config file
    if args.config:
        create_config()

    # check if config file is exists
    if not os.path.isfile('config.json'):
        try:
            to_create = input(
                'Cannot found \'config.json\', do you want to create a new one? (Y/n): ').lower()
            if to_create not in ['n', 'no']:
                create_config()
        except EOFError:
            print('\nGood bye!')
            sys.exit(0)

    # get config file
    try:
        with open('config.json', 'r') as config_file:
            config = json.load(config_file)
            if 'username' in config:
                username = config['username']
            if 'password' in config:
                password = config['password']
            if 'interval' in config:
                time_repeat = config['interval']
    except FileNotFoundError:
        pass

    # get username and password from args
    if args.username is not None:
        username = args.username
    if args.password is not None:
        password = args.password

    # get heartbeat interval from args
    if args.interval is not None:
        time_repeat = args.interval

    # get maximum login attempt from args
    if args.max_attempt is not None:
        max_login_attempt = args.max_attempt

    # check if username and password are provided
    if username == '' or password == '' or username is None or password is None:
        print('Error! Please provide username and password...')
        sys.exit(1)

    init()

    # print login information
    print_format('Logging in with username \'{}\'...'.format(username))

    while True:
        start()
